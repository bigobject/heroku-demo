<?php

$users = Array(
    Array(1, 'M', 'Taiwan'),
    Array(2, 'M', 'US'),
    Array(3, 'F', 'Sleepy Hollow'),
    Array(4, 'F', 'Taiwan'),
    Array(5, 'M', 'Japan'),
    Array(6, 'F', 'United Kindom'),
    Array(7, 'F', 'Scott'),
    Array(8, 'F', 'Happy Tree Valley')
);

$shoes = Array(
    Array(1, 'Toms'),
    Array(2, 'Sperry'),
    Array(3, 'Kate'),
    Array(4, 'Sofi'),
);

#
# Pull in require our Big Object Serivce php client
#
require_once 'vendor/autoload.php';

$newline = (PHP_SAPI == 'cli') ? PHP_EOL : '<br />';
$host = parse_url(getenv('BIGOBJECT_URL'), PHP_URL_HOST);
$port = 9090;


#
# Create a connection object
#
$conn = new bosrv\Connect();

#
# perform connect and get back Multi-Dimensional Analysis
# service client
#
$cli = $conn->connect($host, $port);

#
# START OF TEST DATA GENERATION
#
# Lets prepare a few tables by BigObject SQL
# Dimension tables have a similar syntax to other Sql langeuages
#
# Fact table marks fact values by providing "FACT" in the create spec
# In addition, the relationship should be defined by specifying keys from
# dimension table in order for us to understand how "JOIN" should be done
#
$sql_stmts = Array(
    "CREATE DIM TABLE users.bt (id INT, gender STRING, country STRING, KEY (id))",
    'CREATE DIM TABLE shoes.bt (id INT, brand STRING, KEY(id))',
    "CREATE TABLE steps.bt (users.id INT, shoes.id INT, FACT step INT, DIM (users 'users.bt', shoes 'shoes.bt'))"
);
foreach ($sql_stmts as $create_stmt)
{
    $cli->sql_execute($create_stmt, True);
}

#
# Now, lets put in some data into our table
#
$insert_stmt = 'INSERT INTO users.bt VALUES '.implode(' ', array_map(
    function ($val) { return '('.implode(',', $val).')'; },
    $users
));
$cli->sql_execute($insert_stmt, True);

$insert_stmt = 'INSERT INTO shoes.bt VALUES '.implode(' ', array_map(
    function ($val) { return '('.implode(',', $val).')'; },
    $shoes
));
$cli->sql_execute($insert_stmt, True);

#
# Now, we insert into fact table for a step counts about how different
# people walk with differnt shoes
#
# Suppose we have 10000 records
#
$step_grp_val = 'INSERT INTO steps.bt VALUES '.implode(' ', array_map(
    function ($val) {
        return '('.implode(',', $val).')';
    },
    array_map(
        function ($cnt) {
            global $users, $shoes;
            return Array(
                $users[array_rand($users)][0], // uid
                $shoes[array_rand($shoes)][0], // sid
                rand(1, 10) // step
            );
        },
        range(1, 10000)
    )
));

$start = microtime(true);
$cli->sql_execute($step_grp_val, True);
$end = microtime(true);

print "--------------------------".$newline;
print "Insert time for 10000 values: ".($end - $start).$newline;
print "--------------------------".$newline.$newline;

#
# END OF TEST DATA GENERATION
#
# The above completes the demonstration for Creating and Insert into tables
#


#
# START OF ANALYTIC COMPUTATION DEMO
#
# We have now a few records in our fact table regarding the steps taken at
# random interval from our user base and their preferred sneakers.  We want to
# know the steps taken from each person
#

$select_stmt = 'SELECT SUM(step) FROM steps.bt GROUP BY users.gender, shoes.brand';

$start = microtime(true);
$table = $cli->sql_execute($select_stmt, True);
$end = microtime(true);

print '--------------------------'.$newline;
print $select_stmt.$newline;
print 'Operation took time: '.($end - $start).$newline;
print '--------------------------'.$newline.$newline;

$result_table = Array();
$condition = True;
$rngspec = new bosrv\RangeSpec(Array('page' => 100));

$start = microtime(true);
while(empty($result_table) or $condition)
{
    $row = json_decode($cli->cursor_fetch(
        $table,
        $rngspec
    ));
    $result_table = array_merge($result_table, $row);
    $condition = end($result_table) != -1;
    reset($result_table);
}
$cli->cursor_close($table);
array_pop($result_table);
$end = microtime(true);

print '--------------------------'.$newline;
print 'Time spent to retrieve from cursor: '.($end - $start).$newline;
foreach ($result_table as $row)
{
    print '['.implode(' ', $row).']'.$newline;
}
print '--------------------------'.$newline.$newline;
