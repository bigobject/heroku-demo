<?php

#
# Pull in require our Big Object Serivce php client
#
require_once 'vendor/autoload.php';

$newline = (PHP_SAPI == 'cli') ? PHP_EOL : '<br />';
$host = parse_url(getenv('BIGOBJECT_URL'), PHP_URL_HOST);
$port = 9090;


#
# Create a connection object
#
$conn = new bosrv\Connect();

#
# perform connect and get back Multi-Dimensional Analysis
# service client
#
$cli = $conn->connect($host, $port);

#
# START OF ANALYTIC COMPUTATION DEMO
#
# We have now a few records in our fact table regarding the sales from customer
# and the product they purchased.  We wanted to know what products by which
# gender of our customer base made how much purchases in total
#

$select_stmt = 'SELECT SUM(Data) FROM sales.bt GROUP BY Product.brand, Customer.gender';

$start = microtime(true);
$table = $cli->sql_execute($select_stmt, True);
$end = microtime(true);

print '--------------------------'.$newline;
print $select_stmt.$newline;
print 'Operation took time: '.($end - $start).$newline;
print '--------------------------'.$newline.$newline;

$result_table = Array();
$condition = True;
$rngspec = new bosrv\RangeSpec(Array('page' => 100));

$start = microtime(true);
while(empty($result_table) or $condition)
{
    $row = json_decode($cli->cursor_fetch(
        $table,
        $rngspec
    ));
    $result_table = array_merge($result_table, $row);
    $condition = end($result_table) != -1;
    reset($result_table);
}
$cli->cursor_close($table);
array_pop($result_table);
$end = microtime(true);

print '--------------------------'.$newline;
print 'Time spent to retrieve from cursor:'.($end - $start).$newline;
foreach ($result_table as $row)
{
    print '['.implode(' ', $row).']'.$newline;
}
print '--------------------------'.$newline.$newline;
